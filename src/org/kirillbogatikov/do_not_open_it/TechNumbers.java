package org.kirillbogatikov.do_not_open_it;

import java.util.Scanner;

public class TechNumbers {
    static Scanner scanner = new Scanner(System.in);
    
    public static void main(String[] args) {
        String data = scanner.nextLine();
        try {
            int i = Integer.parseInt(data);
            System.out.println(Integer.toBinaryString(i));
        } catch(NumberFormatException t) {
            double d = Double.parseDouble(data);
            long l = Double.doubleToLongBits(d);
            System.out.println(Long.toBinaryString(l));
        }
    }
}
