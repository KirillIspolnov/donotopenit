package org.kirillbogatikov.do_not_open_it;

import java.util.Scanner;

public class Pirates {
    private static final int MAX_ORDER = 16;
    static Scanner scanner = new Scanner(System.in);
    
    public static void main(String[] args) {
        int K = scanner.nextInt();
        
        long testedNumber = -1;
        boolean testPassed = false;
        for(int i = 1; i < MAX_ORDER; i++) {
            testedNumber = (long)Math.pow(K, i);
            testPassed = true;
            
            for(int j = 2; j < K; j++) {
                testPassed &= (testedNumber % j == 1);
            }
            
            if(testPassed) {
                break;
            }
        }
        
        if(testPassed) {
            System.out.printf("У пиратов было %d камней%n", testedNumber);
        } else {
            System.out.println("Я не смог определить кол-во камней...");
        }
    }

}
