package org.kirillbogatikov.do_not_open_it.numbers_3_5;

import java.util.Arrays;

public class MyArray<T extends Object> {
    private T[] buffer;
    private int length;
    
    public MyArray(int count) {
        buffer = allocate(count);
        length = 0;
    }
    
    public MyArray() {
        this(10);
    }
    
    private T[] allocate(int l) {
        if(buffer == null || l > capacity())
            return (T[])(new Object[l]);
        
        return buffer;
    }
    
    public void add(T e) {
        if(buffer.length == length) {
            T[] buffer1 = allocate(buffer.length + 1);
            for(int i = 0; i < buffer.length; i++) {
                buffer1[i] = buffer[i];
            }
            buffer1[buffer.length] = e;
            buffer = buffer1;
        } else {
            buffer[length] = e;
        }
        
        length++;
        System.gc();
    }
    
    public T remove(int index) {
        T value = buffer[index];
        for(int i = index; i < buffer.length - 1; i++) {
            buffer[i] = buffer[i + 1];
        }
        length++;
        System.gc();
        return value;
    }
    
    public T get(int index) throws ArrayIndexOutOfBoundsException {
        return buffer[index];
    }
    
    public void sort() {
        Arrays.sort(buffer);
    }
    
    public int length() {
        return length;
    }
    
    public int capacity() {
        return buffer.length;
    }
    
    public boolean isEmpty() {
        return length == 0;
    }
    
    public String toString() {
        StringBuilder b = new StringBuilder("[");
        for(int i = 0; i < length; i++) {
            b.append(buffer[i].toString()).append(", ");
        }
        if(b.length() > 2)
            b.setLength(b.length() - 2);
        
        b.append("]");
        return b.toString();
    }
}
