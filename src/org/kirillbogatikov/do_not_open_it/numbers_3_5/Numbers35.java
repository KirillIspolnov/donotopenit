package org.kirillbogatikov.do_not_open_it.numbers_3_5;

import java.util.Scanner;

public class Numbers35 {
    static Scanner scanner = new Scanner(System.in);
    
    public static void main(String[] args) {
        System.out.print("Введите число элементов: ");
        int count = scanner.nextInt();
        
        MyArray<Integer> initial = new MyArray<>(count);
        MyArray<Integer> multi3 = new MyArray<>();
        MyArray<Integer> multi5 = new MyArray<>();
        MyArray<Integer> multi35 = new MyArray<>();
        
        System.out.printf("Введите %d целых чисел: ", count);
        int number;
        for(int i = 0; i < count; i++) {
            number = scanner.nextInt();
            initial.add(number);
            
            if(number % 3 == 0)
                multi3.add(number);
            
            if(number % 5 == 0)
                multi5.add(number);
            
            if((number % 3) == 0 && (number % 5) == 0) {
                multi35.add(number);
            }
        }
        
        if(!multi3.isEmpty())
            System.out.printf("Числа, кратные 3: %s%n", multi3);
        else
            System.out.print("Чисел, кратных 3, нет");
        
        if(!multi5.isEmpty())
            System.out.printf("Числа, кратные 5: %s%n", multi5);
        else
            System.out.print("Чисел, кратных 5, нет");
        
        if(!multi35.isEmpty())
            System.out.printf("Числа, кратные и 3, и 5: %s%n", multi35);
        else
            System.out.print("Чисел, кратных и 3, и 5 одновременно, нет");
        
    }
    
}
